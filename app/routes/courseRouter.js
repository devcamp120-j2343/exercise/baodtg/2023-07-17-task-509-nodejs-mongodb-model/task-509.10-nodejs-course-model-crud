//Khai báo thư viện expres
const express = require('express');


//Khai báo controller
const courseController = require('../controllers/course.controller')

//khai báo middleware
const {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware,
} = require('../middlewares/couresMiddleware');



//tạo ra router:
const courseRouter = express.Router();

courseRouter.get('/courses', courseController.getAllCourse)
courseRouter.get('/courses/:courseId', courseController.getCourseById)

courseRouter.post('/courses', courseController.createCourse)

courseRouter.put('/courses/:courseId',putCoursesMiddleware, courseController.updateCourseById)

courseRouter.delete('/courses/:courseId',deleteCoursesMiddleware, courseController.deleteCourseById)


module.exports = {courseRouter}