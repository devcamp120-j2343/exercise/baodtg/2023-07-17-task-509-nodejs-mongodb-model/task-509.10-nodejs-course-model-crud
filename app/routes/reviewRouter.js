//Khai báo thư viện expres
const express = require('express');

//khai báo middleware
const {
    getAllReviewMiddleware,
    getReviewMiddleware,
    postReviewMiddleware,
    putReviewMiddleware,
    deleteReviewMiddleware,
} = require('../middlewares/reviewMiddleware');

//tạo ra router:
const reviewRouter = express.Router();
reviewRouter.use(getAllReviewMiddleware);

reviewRouter.get('/reviews',getAllReviewMiddleware, (req, res)=> {
    res.json({
        message: 'Get all reviews'
    })
})
reviewRouter.get('/reviews/:reviewId', getReviewMiddleware, (req, res)=> {
    let reviewId = req.params.reviewId
    res.json({
        message: `Get reviews id = ${reviewId}`
    })
})

reviewRouter.post('/reviews',postReviewMiddleware, (req, res)=> {
    res.json({
        message: `Create new review`
    })
})

reviewRouter.put('/reviews/:reviewId',putReviewMiddleware, (req, res)=> {
    let reviewId = req.params.reviewId
    res.json({
        message: `Update review id = ${reviewId}`
    })
})

reviewRouter.delete('/reviews/:reviewId',deleteReviewMiddleware, (req, res)=> {
    let reviewId = req.params.reviewId
    res.json({
        message: `Delete review id = ${reviewId}`
    })
})


module.exports = {reviewRouter}