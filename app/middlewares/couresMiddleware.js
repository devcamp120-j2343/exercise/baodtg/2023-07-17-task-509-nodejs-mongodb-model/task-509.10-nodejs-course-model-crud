const getAllCoursesMiddleware = (req, res, next) => {
    console.log("Get All Courses!!!");
    next();
}

const getCoursesMiddleware = (req, res, next) => {
    console.log("Get Courses!");
    next();
}

const postCoursesMiddleware = (req, res, next) => {
    console.log("Create Courses!");
    next();
}

const putCoursesMiddleware = (req, res, next) => {
    console.log("Update Courses!");
    next();
}

const deleteCoursesMiddleware = (req, res, next) => {
    console.log("Delete Courses!");
    next();
}

module.exports = {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware,
}