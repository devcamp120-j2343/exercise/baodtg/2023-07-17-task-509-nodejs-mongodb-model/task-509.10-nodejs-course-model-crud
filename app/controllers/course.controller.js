//import models
const { default: mongoose } = require('mongoose')
const courseModel = require('../models/courseModel')


const getAllCourse = (req, res) => {
    courseModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: `Get all course successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any course`,
                    data
                })
            }
        })
}

const getCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    let courseId = req.params.courseId;
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model:
    courseModel.findById(courseId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: `Get course by id succesfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any course`,
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })

}

const createCourse = (req, res) => {
    //B1: thu thập dữ liệu:
    let body = req.body;
    //B2: kiểm tra dữ liệu:
    if (!body.title) {
        return res.status(400).json({
            message: `title is required!`
        })
    }
    if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        return res.status(400).json({
            message: `noStudent is invalid!`
        })
    }
    //B3: xử lý và trả kết quả:
    let newCourse = new courseModel({
        _id: new mongoose.Types.ObjectId(),
        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    })

    courseModel.create(newCourse)
        .then((data) => {
            return res.status(201).json({
                status: "Create new course sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })



}

const updateCourseById = (req, res) => {
    //B1: thu thập dữ liệu:
    let courseId = req.params.courseId;
    const { title, description, noStudent } = req.body;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Id is invalid`
        })
    }
    if (!title) {
        return res.status(400).json({
            status: `Bad request`,
            message: `title is required!`
        })
    }
    if (isNaN(noStudent) || noStudent < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: `noStudent is invalid!`
        })
    }
    //B3:Thực thi model:
    let updateCourse = {
        title,
        description,
        noStudent
    }

    courseModel.findByIdAndUpdate(courseId, updateCourse)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: `Update course successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any course`,
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })

}

const deleteCourseById = (req, res) => {
    //B1: thu thập dữ liệu:
    let courseId = req.params.courseId
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Id is invalid`
        })
    }
    //B3:Thực thi model:
    courseModel.findByIdAndDelete(courseId)
        .then((data) => {
            if(data) {
                return res.status(200).json({
                    status: `Delete course successfully`,
                    data
                })
            }else{
                return res.status(404).json({
                    status: `Not found any course`,
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })
}

module.exports = { getAllCourse, getCourseById, createCourse, updateCourseById, deleteCourseById }