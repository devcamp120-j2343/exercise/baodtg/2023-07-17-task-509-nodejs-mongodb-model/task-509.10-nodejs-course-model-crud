//Khai báo thư viện express:
const express = require("express");

//Khai báo router
const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");

//Khai báo thư viện mongoose:
const mongoose = require('mongoose')

//KHai báo model mongoose:
const reviewModel = require('./app/models/reviewModel')
const courseModel = require('./app/models/courseModel')



//Khai báo app
const app = express();
app.use(express.json())
//Khai báo port
const port = 8000;

//kết nối mongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error))

const methodMiddleWare = (req, res, next) => {
    console.log(req.method);
    next()
};
app.use((req, res, next) => {
    console.log(new Date());
    next();
}, methodMiddleWare
)

app.post('/', (req, res, next) => {
    console.log(req.method);
    next()
})

app.post('/', (req, res) => {
    console.log(`Post method`)

    res.status(200).json({

        message: `Post method`,

    })
})

app.get('/', (req, res) => {
    let today = new Date();
    console.log(`To day is ${today.getDate()} ${today.getMonth()} ${today.getFullYear()}`),

        res.status(200).json({

            message: `To day is ${today.getDate()} ${today.getMonth()} ${today.getFullYear()}`,

        })
})


app.use('/', courseRouter)

app.use('/', reviewRouter)


//Khởi động app:
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})

